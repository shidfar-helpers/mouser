all:
	mkdir build &&\
	cd build &&\
	cmake .. &&\
	make all &&\
	mv ./mouser ../
clean:
	rm -rf build
	rm mouser
